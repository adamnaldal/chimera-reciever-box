# Chimera-receiver-box

A 3d printable box for the receiver of the chimera ergo 42 mechanical keyboard.  
Including 2 holes for acces to the buttons on the receiver and one hole to allow  
the led to shine throught if you don't have transparent printing material.

Sucessfully printed with transparent PLA with the following settings:
* 80% infill
* 0.1 layer thickness (also called "fine" in cura)
* Supports printed with the same PLA as the rest of the object.

Read on for built tips and more images.

![Image of the box with the led light showing](images/profile_dim.jpg)

## Build tips
The top and bottom snap together very tightly, so tightly that you might want to  
sand the edge of the top part to make it click into the bottom a little more easily.

If you feel like it you can glue the two parts together, but in my experience it  
is not necessary. Even with sanding i can't pry the two parts apart without tools.

The box is intentionally made quite spacious on the inside to not put anny  
pressure on the electronics. This means that the receiver rattles around a bit in  
the case. Therefore i recommend glueing the elite-c part of the receiver to the  
bottom of the box with a glue gun or similar.

## Gallery
![usb port image](images/usb_port_dim.jpg)
![usb port with usb connected](images/usb_port_bright.jpg)
![top down image](images/top_down_bright.jpg)
![3d model image](images/3d_model_image.png)
